<?php


namespace Velmie\WalletDiscovery;


class Url
{
    private $url;
    private $scheme;

    /**
     * Url constructor.
     * @param $url
     * @param $scheme
     */
    public function __construct($url, $scheme = null)
    {
        $this->url = $url;
        $this->scheme = $scheme;
    }


    public function __toString(): string
    {
        return $this->scheme . '://' . $this->url . '/';
    }

    /**
     * @param mixed|null $scheme
     */
    public function setScheme(string $scheme): void
    {
        $this->scheme = $scheme;
    }
}