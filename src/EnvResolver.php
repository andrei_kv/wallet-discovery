<?php


namespace Velmie\WalletDiscovery;


class EnvResolver implements Resolver
{
    /**
     *
     */
    private const envVarNameValidCharactersStr = '/[^a-zA-Z0-9_]+/';

    /**
     * @param string $portName
     * @param string $serviceName
     * @return false|Url
     */
    public function resolve(string $portName, string $serviceName)
    {
        $transformedServiceName = $this->envNormalizeName($serviceName);
        $transformedPortName = $this->envNormalizeName($portName);

        $envVarPortName = sprintf('%s_SERVICE_PORT_%s', $transformedServiceName, $transformedPortName);
	    $envVarServiceName = sprintf("%s_SERVICE_HOST", $transformedServiceName);

	    $host = getenv($envVarServiceName);
	    if ($host == false) {
	        return false;
        }

	    $port = getenv($envVarPortName);
        if ($port == false) {
            return false;
        }

        return new Url(sprintf('%s:%s', $host, $port));
    }

    /**
     * @param string $name
     * @return string|null
     */
    private function envNormalizeName(string $name)
    {
        $name = str_replace('-', '_', $name);
        $name = strtoupper($name);
        $name = preg_replace(self::envVarNameValidCharactersStr,'', $name);
        return $name;
    }
}