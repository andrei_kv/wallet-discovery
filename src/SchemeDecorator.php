<?php


namespace Velmie\WalletDiscovery;


class SchemeDecorator implements Resolver
{
    /**
     * @var Resolver
     */
    private $resolver;
    /**
     * @var array
     */
    private $portNameScheme;

    /**
     * SchemeDecorator constructor.
     * @param $resolver
     * @param $portNameScheme
     */
    public function __construct(Resolver $resolver, array $portNameScheme)
    {
        $this->resolver = $resolver;
        $this->portNameScheme = $portNameScheme;
    }


    /**
     * @param string $portName
     * @param string $serviceName
     * @return mixed
     */
    public function resolve(string $portName, string $serviceName)
    {
        if ($this->portNameScheme[$portName]) {
            $url = $this->resolver->resolve($portName, $serviceName);
            if (!$url) {
                $url->setScheme($this->portNameScheme[$portName]);
            }
            return $url;
        }
        return $this->resolver->resolve($portName, $serviceName);
    }
}