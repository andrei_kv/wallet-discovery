<?php


namespace Velmie\WalletDiscovery;


/**
 * Class DNSResolver
 * @package Velmie\WalletDiscovery
 */
class DNSResolver implements Resolver
{
    /**
     * @var string
     */
    private $proto;

    /**
     * @var
     */
    private $records;

    /**
     * DNSResolver constructor.
     */
    public function __construct(string $proto = 'tcp')
    {
        if ($proto != 'tcp' && $proto != 'udp') {
            $proto = 'tcp';
        }
        $this->proto = $proto;
    }

    /**
     * @param string $portName
     * @param string $serviceName
     * @return false|Url
     */
    public function resolve(string $portName, string $serviceName)
    {
        $target = "_" .$portName . "._" .$this->proto . "." . $serviceName;
        $this->records = dns_get_record($target, DNS_SRV);
        if ($this->records != false) {
           $this->orderRecords();
           return new Url(sprintf('%s:%d', $this->records[0]['target'], $this->records[0]['port']), $this->proto);
        }
        return false;
    }

    // sort reorders SRV records as specified in RFC 2782.

    /**
     *
     */
    private function orderRecords()
    {
        uasort($this->records, array($this, 'sortRecords'));
        $i = 0;
        for ($j = 1; $j < sizeof($this->records); $j++) {
            if ($this->records[$i]['pri'] != $this->records[$j]['pri']) {
                $this->shuffle($i, $j);
                $i = $j;
            }
        }
        $this->shuffle($i, sizeof($this->records));
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    private function sortRecords($a, $b)
    {
        if ($a['pri'] == $b['pri']) {
            if($a['weight'] == $b['weight']) {
                return 0;
            }
            return ($a['weight'] < $b['weight']) ? -1 : 1;
        }
        return ($a['pri'] < $b['pri']) ? -1 : 1;
    }

    // shuffleByWeight shuffles SRV records by weight using the algorithm
    // described in RFC 2782.
    /**
     * @param int $i
     * @param int $j
     * @throws \Exception
     */
    private function shuffle(int $i, int $j)
    {
        $sum = 0;
        for ($k = $i; $k < $j; $k++) {
            $sum += $this->records[$k]['weight'];
        }
        while (($sum > 0) && (($j - $i + 1) > 1)) {
            $s = 0;
            $n = random_int(0, $sum-1);
            for ($l = $i; $k < $j; $k++) {
                $s += $this->records[$l]['weight'];
                if ($s > $n) {
                    if ($i > 0) {
                        $temp = $this->records[0];
                        $this->records[0] = $this->records[$l];
                        $this->records[$l] = $temp;
                    }
                    break;
                }
            }
            $sum -= $this->records[0]['weight'];
            $i++;
        }
    }
}