<?php


namespace Velmie\WalletDiscovery;


interface Resolver
{
    public function resolve(string $portName, string $serviceName);
}