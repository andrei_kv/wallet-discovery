<?php


namespace Velmie\WalletDiscovery;


class FallBackResolver implements Resolver
{
    /**
     *
     */
    const maxCalls = 128;

    /**
     * @var Resolver[]
     */
    private $resolvers;

    /**
     * FallbackResolver constructor.
     * @param $resolvers
     */
    public function __construct(Resolver ...$resolvers)
    {
        $this->resolvers = $resolvers;
    }

    /**
     * @param string $portName
     * @param string $serviceName
     * @return false|Url
     */
    public function resolve(string $portName, string $serviceName)
    {
        $c = 0;
        foreach ($this->reslovers as $resolver) {
            $c++;
            if ($c > self::maxCalls) {
                return false;
            }
            $serviceUrl = $resolver->resolve($portName, $serviceName);
            if ($serviceUrl != false) {
                return $serviceUrl;
            }
        }
        return false;
    }
}